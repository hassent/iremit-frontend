import { Component, OnInit } from '@angular/core';
import { GlobalService } from '@iRemitt/services/global.service';

@Component({
  selector: 'app-home',
  templateUrl: '../../../views/home.html',
  styles: []
})
export class HomeComponent implements OnInit {

  constructor(private global:GlobalService) { }

  ngOnInit() {
  }

  toggleProgress() {
    this.global.isOnProgress = !this.global.isOnProgress;
  }

}
