import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { ApiService } from '@iRemitt/services/api.service';
import { environment } from 'environments/environment';
import { API } from '@iRemitt/models/endpoints';
import { Sanction } from '@iRemitt/models/sanction';
import { GlobalService } from '@iRemitt/services/global.service';
import { Subscription } from 'rxjs';
import { SanctionDialogComponent } from '@iRemitt/page/dialog/sanction-dialog.component';
import { Stab } from '@iRemitt/models/stab';

@Component({
  selector: 'app-sanction',
  templateUrl: '../../../views/sanction.html',
  styles: []
})
export class SanctionComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  private baseUrl = environment.baseUrl;
  private displayedColumns = ['id', 'fullName', 'country'];
  private dataSource = new MatTableDataSource<Sanction>();
  public sanction: Sanction;
  private subscriptions: Subscription[] = [];
  private queryParams: { limit: 25, orderBy: 'fName', page: 1 };
  constructor(private api: ApiService, public global: GlobalService, private dialog: MatDialog) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.global.isOnProgress = true;
    this.subscriptions.push(this.requestData()
      .map(data => { return data["sanctions"] as Sanction[] })
      //.map(el => { return el['fullName']=el['firstName']})
      .subscribe(data => {
        data.forEach(e => {return e['fullName']=`${e['firstName']} ${e['middleName']} ${e['lastName']}`});
        this.dataSource.data = data;
        this.global.isOnProgress = false;
      }));
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getQueryParam(limit: number, order: string, page: number): string {
    return `${this.baseUrl}${API.SANCTION_URL}?limit=${limit}&order=${order}&page=${page}`;
  }

  requestData(limit: number = 25, order: string = 'fName', page: number = 1) {
    return this.api.get(this.getQueryParam(limit, order, page));
  }

  loadData() {

  }


  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  getObject(row: Sanction) {
    this.sanction = row;
  }

  openDialog(sanction?:Sanction): void {
    let dialogRef = this.dialog.open(SanctionDialogComponent, {
      data: { sanction: sanction || Stab.sanction }
    });

    dialogRef.afterClosed().subscribe(sanction => {
      console.log('The dialog was closed', sanction);

    });
  }

  ngOnDestroy() {
    this.subscriptions.map(s => s.unsubscribe());
  }



}



