import { Component, Input } from '@angular/core';
import { ValidationService } from '@iRemitt/services/validation.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'control-messages',
  template: `<div *ngIf="errorMessage !== null">{{errorMessage}}</div>`,
  styles: []
})
export class ControlMessageComponent {

  _errorMessage: string;
  @Input() control: FormControl;
  constructor() { }

  get errorMessage():string {
    for (let propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
        return ValidationService.getValidatorErrorMessage(propertyName, this.control.errors[propertyName]);
      }
    }

    return null;
  }

}
