import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { RemittanceComponent } from './remittance/remittance.component';
import { DepositComponent } from './deposit/deposit.component';
import { SanctionComponent } from './sanction/sanction.component';
import { MaterialModule } from '@iRemitt/material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { DeferLoadModule } from '@trademe/ng-defer-load';
import { SanctionDialogComponent } from '@iRemitt/page/dialog/sanction-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ControlMessageComponent } from './control-message/control-message.component';
import { ValidationService } from '@iRemitt/services/validation.service';


@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    HttpClientModule,
    DeferLoadModule,
    ReactiveFormsModule
  ],
  declarations: [
    HomeComponent, UserComponent, RemittanceComponent, DepositComponent,
    SanctionComponent, SanctionDialogComponent, ControlMessageComponent],
  exports: [HomeComponent],
  providers: [ValidationService],
  entryComponents: [SanctionDialogComponent]
})
export class PageModule { }
