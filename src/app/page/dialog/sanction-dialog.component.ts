import {Http} from '@angular/http';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { Sanction } from '@iRemitt/models/sanction';

@Component({
  selector: 'app-sanction-dialog',
  templateUrl: '../../../views/dialogs/sanction.html',
  styles: []
})
export class SanctionDialogComponent implements OnInit {
  countries:any;
  f:FormGroup;
  constructor(
    public dialogRef: MatDialogRef<SanctionDialogComponent>,
    public http:Http,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.http.get('assets/countries.json').subscribe(
        res => this.countries = res.json(),
      );
     }

  onNoClick(): void {
    this.dialogRef.close(this.f.value);
  }

  ngOnInit() {
    const sanction:Sanction = this.data.sanction;
    this.f = new FormGroup({
      firstName: new FormControl(sanction.firstName,[
        Validators.required,
        Validators.minLength(4),
      ]),
      middleName: new FormControl(sanction.middleName),
      lastName: new FormControl(sanction.lastName),
      country: new FormControl(sanction.country)
    });
    }
  onSubmit(){
    console.log(this.f.value);
  }
}

