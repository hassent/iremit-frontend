import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '@iRemitt/page/home/home.component';
import { SanctionComponent } from '@iRemitt/page/sanction/sanction.component';
import { AuthGuard } from '@iRemitt/services/auth.guard';
import { LoginComponent } from '@iRemitt/core/login/login.component';
import { UserComponent } from '@iRemitt/page/user/user.component';

const routes: Routes = [
  {path:'', component:HomeComponent},
  {path:'login', component:LoginComponent},
  {path:'sanction', component:SanctionComponent, canActivate:[AuthGuard]},
  {path:'user', component:UserComponent, canActivate:[AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
