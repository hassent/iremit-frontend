import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { GlobalService } from '@iRemitt/services/global.service';
import { AuthService } from '@iRemitt/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: '../../../views/header.html',
  styles: []
})
export class HeaderComponent implements OnInit {
  @Output() navToggle = new EventEmitter<boolean>();
  navOpen() {
    this.navToggle.emit(true);
  }
  constructor(
    public auth:AuthService,
    public global:GlobalService) { }

  ngOnInit() {
  }

}
