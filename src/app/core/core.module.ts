import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MaterialModule } from '@iRemitt/material/material.module';
import { AuthService } from '@iRemitt/services/auth.service';
import { ApiService } from '@iRemitt/services/api.service';
import { HttpModule } from '@angular/http';
import { GlobalService } from '@iRemitt/services/global.service';
import { LoginComponent } from '@iRemitt/core/login/login.component';
import { StorageService } from '@iRemitt/services/storage.service';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    HttpModule,
  ],
  declarations: [HeaderComponent, FooterComponent, LoginComponent, LoadingSpinnerComponent],
  exports: [HeaderComponent,FooterComponent, LoginComponent],
  providers: [AuthService,ApiService,GlobalService, StorageService]
})
export class CoreModule { }
