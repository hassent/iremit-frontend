import { Component, OnInit } from '@angular/core';
import { AuthService } from '@iRemitt/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: '../../../views/login.html',
  styles: []
})
export class LoginComponent implements OnInit {

  constructor(public auth:AuthService) { }

  ngOnInit() {
  }

}
