import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {
  constructor() { }

  public storeObject(key: string, value: any): void {
    localStorage.setItem(key, value);

  }

  public getObject(key: string): any {
   return localStorage.getItem(key);
  }
}
