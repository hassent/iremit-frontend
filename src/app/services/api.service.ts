import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  get(url: string, options?: any) {
    return this.http.get(url, httpOptions)
  }


  post(url: string, body: any) {
    return this.http.post(url, body)
  }

  update(url: string, id: string, body: any) {
    return this.http.put(`${url}/${id}`, body)
  }

  delete(url: string, id: string) {
    return this.http.delete(`${url}/${id}`)
  }
  patch(url: string, id: string, body: any) {
    return this.http.patch(`${url}/${id}`, body)
  }

  private extractData(res: Response) {
    console.log(res.json());
    let body = res.json();
    return body;
  }
  private handleError(error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.status);
  }

}

