import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { firebase } from '@firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/switchMap';
import { StorageService } from '@iRemitt/services/storage.service';

interface User {
  uid: string;
  email: string;
  photoURL?: string;
  displayName?: string;
  status?: UserStatus;
  referee?: string;
  token?: string;
}
enum UserStatus {
  PENDING = "PENDING",
  ACTIVE = "ACTIVE"
}


@Injectable()
export class AuthService {

  user: Observable<User>;


  constructor(private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    private storage: StorageService) {
    afs.firestore.settings({ timestampsInSnapshots: true });
    //// Get auth data, then get firestore user document || null
    this.user = this.afAuth.authState
      .switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges()
        } else {
          return Observable.of(null)
        }
      });

  }



  login() {
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }

  private oAuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
      .then((credential) => {
        this.updateUserData(credential.user).then(

          () => this.router.navigate([this.storage.getObject("redirectUrl")] || ['/'])
        )
      })
  }

  private async updateUserData(user) {
    // Sets user data to firestore on login
    let data: User;
    const userRef: AngularFirestoreDocument<any> = await this.afs.doc(`users/${user.uid}`);
    const docSnapShot = await this.afs.firestore.doc(`users/${user.uid}`).get();
    if (docSnapShot.exists) {
      data = {
        uid: user.uid,
        email: user.email,
        displayName: user.displayName,
        photoURL: user.photoURL,
      }
    } else {
      data = {
        uid: user.uid,
        email: user.email,
        displayName: user.displayName,
        photoURL: user.photoURL,
        status: UserStatus.PENDING,
        referee: 'hassicho@gmail.com',
      }
    }
    return userRef.set(data, { merge: true });

  }


  logout() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(['/']);
    });
  }
}