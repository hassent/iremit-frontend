import { Component, AfterViewInit } from '@angular/core';
import { Router, NavigationStart, NavigationCancel, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: '../views/app.html',
  styles: []
})
export class AppComponent implements AfterViewInit {

  title = 'iRemit';
  loading;
  navLists = [
    { "name": "Customer", "icon": "supervised_user_circle", "href": "/customer" },
    { "name": "Remittance", "icon": "attach_money", "href": "/remittance" },
    { "name": "Deposit", "icon": "account_balance_wallet", "href": "/deposit" },
    { "name": "Report", "icon": "assessment", "href": "/report" },
    { "name": "Sanction", "icon": "pan_tool", "href": "/sanction" },
    { "name": "Users", "icon": "people", "href": "/user" },
    { "name": "Admin", "icon": "account_circle", "href": "/admin" },
  ];
  constructor(
    private router: Router
  ) {
    this.loading = true;
  }
  ngAfterViewInit() {
    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationStart) {
          this.loading = true;
        }
        else if (
          event instanceof NavigationEnd ||
          event instanceof NavigationCancel
        ) {
          this.loading = false;
        }
      });
  }
}
