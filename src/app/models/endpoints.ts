export enum API {
    CUSTOMER_URL = 'customers',
    REMITTANCE_URL = 'remittances',
    USER_URL = 'users',
    DEPOSIT_URL = 'deposits',
    SANCTION_URL = 'sanctions'
}