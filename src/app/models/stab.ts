import { Sanction } from "@iRemitt/models/sanction";

export class Stab {
    static sanction: Sanction = {
        firstName: '',
        middleName: '',
        lastName: '',
        country: 'Sweden'
    }
}