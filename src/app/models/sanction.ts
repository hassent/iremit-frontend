import { Entity } from "@iRemitt/models/entity";

export interface Sanction extends Entity {
    firstName:string,
    middleName:string,
    lastName:string,
    country:string
}