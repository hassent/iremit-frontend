import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Observable } from "rxjs/Observable";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { catchError, finalize } from "rxjs/operators";
import { of } from "rxjs/observable/of";
import { Entity } from "@iRemitt/models/entity";
import { ApiService } from "@iRemitt/services/api.service";
import { environment } from "environments/environment";
import { MatTableDataSource } from "@angular/material";
import { Sanction } from "@iRemitt/models/sanction";


export class TableData<T extends Entity> extends MatTableDataSource<T>{
    private baseUrl = environment.baseUrl;

    private dataSubject = new BehaviorSubject<T[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private api: ApiService) { super() }

    connect(): BehaviorSubject<T[]> {
        return this.dataSubject;
    }

    disconnect(): void {
        this.dataSubject.complete();
        this.loadingSubject.complete();
        const sub = this.dataSubject
        .map(val => { return val })

            .subscribe(
                (val) => console.log(val),
                (err) => console.log(err),
                () => console.log('completed')
            );
        sub.unsubscribe();
        const obs = Observable.create({

        });
        obs.subscribe()

    }

    getData(endpoint: string, label?: string) {
        this.loadingSubject.next(true);

        this.api.get(this.baseUrl + endpoint).pipe(
            catchError(() => of([])),
            finalize(() => this.loadingSubject.next(false))
        )
            .subscribe(data => this.dataSubject.next(data[label] as T[]));
    }
}