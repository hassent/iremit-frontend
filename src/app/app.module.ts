import 'hammerjs';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from '@iRemitt/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from '@iRemitt/app.component';
import { AngularFireModule } from 'angularfire2';
import { CoreModule } from '@iRemitt/core/core.module';
import { PageModule } from '@iRemitt/page/page.module';
import { MaterialModule } from '@iRemitt/material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from 'environments/environment';
import { AuthGuard } from '@iRemitt/services/auth.guard';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    CoreModule,
    PageModule,
    MaterialModule,
    AngularFireModule.initializeApp(environment.firebaseConfig,'iRemitt')
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
