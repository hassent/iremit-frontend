// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyB80tr1Y03KinTL3WQzGJ4bedqRVbqzlRM",
    authDomain: "iremit-isheger.firebaseapp.com",
    databaseURL: "https://iremit-isheger.firebaseio.com",
    projectId: "iremit-isheger",
    storageBucket: "iremit-isheger.appspot.com",
    messagingSenderId: "1052178686624"
  },
  baseUrl : 'http://localhost:5000/iremit-isheger/us-central1/api/v1/'
};
